<?php

return [
		'user-management' => [		'title' => 'User Management',		'created_at' => 'Time',		'fields' => [		],	],
		'roles' => [		'title' => 'Roles',		'created_at' => 'Time',		'fields' => [			'title' => 'Title',		],	],
		'users' => [		'title' => 'Users',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'email' => 'Email',			'password' => 'Password',			'role' => 'Role',			'remember-token' => 'Remember token',		],	],
		'clients' => [		'title' => 'Clients',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'surname' => 'Surname',			'email' => 'Email',			'phone' => 'Phone',		],	],
		'employees' => [		'title' => 'Employees',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'surname' => 'Surname',		],	],
		'services' => [		'title' => 'Services',		'created_at' => 'Time',		'fields' => [			'title' => 'Title',			'price' => 'Price',			'description' => 'Description',		],	],
		'appointments' => [		'title' => 'Appointments',		'created_at' => 'Time',		'fields' => [			'client' => 'Client',			'employee' => 'Employee',			'service' => 'Service',			'date' => 'Date',			'start' => 'Start',			'end' => 'End',		],	],
		'calendar' => [		'title' => 'Calendar',		'created_at' => 'Time',		'fields' => [		],	],
	'custom_controller_index' => 'Персонализиран контролер.',
	'quickadmin_title' => 'LaraScheduler2',
];