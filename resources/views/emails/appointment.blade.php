@component('mail::message')
# Appointment information

You have appointment with {{$appointment -> employee -> name . " " . $appointment -> employee -> surname}} at {{$appointment -> date}} from {{$appointment -> start. " to ". $appointment -> end}}

Service: {{$appointment -> service -> title}}

Description: {{$appointment -> service -> description}}

Cost: {{$appointment -> service -> cost}}


@endcomponent
