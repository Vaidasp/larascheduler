@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.appointments.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.appointments.fields.client')</th>
                            <td>{{ $appointment->client->name or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.appointments.fields.employee')</th>
                            <td>{{ $appointment->employee->name or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.appointments.fields.service')</th>
                            <td>{{ $appointment->service->title or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.appointments.fields.date')</th>
                            <td>{{ $appointment->date }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.appointments.fields.start')</th>
                            <td>{{ $appointment->start }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.appointments.fields.end')</th>
                            <td>{{ $appointment->end }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.appointments.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop