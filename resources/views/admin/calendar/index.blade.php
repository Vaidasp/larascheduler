@extends('layouts.app')

@section('content')
  @can('appointment_create')
    <h3>Calendar</h3>
  <p>
      <a href="{{ route('admin.appointments.create') }}" class="btn btn-success">Add appointment</a>

  </p>
  @endcan
<div id="calendar">

</div>
@stop

@section('scripts')
    <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>

@endsection

@section('javascript')
    <script>
        @can('client_delete')
            window.route_mass_crud_entries_destroy = '{{ route('admin.clients.mass_destroy') }}';
        @endcan

    </script>
@endsection
