<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Service
 *
 * @package App
 * @property string $title
 * @property decimal $price
 * @property string $description
*/
class Service extends Model
{
    use SoftDeletes;

    protected $fillable = ['title', 'price', 'description'];
    

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setPriceAttribute($input)
    {
        $this->attributes['price'] = $input ? $input : null;
    }
    
}
