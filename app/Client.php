<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Client
 *
 * @package App
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property string $phone
*/
class Client extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'surname', 'email', 'phone'];
    
    
}
