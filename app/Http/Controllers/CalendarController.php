<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Appointment;

class CalendarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.calendar.index');
    }

    public function getEventsJson()
    {
        $appointments = Appointment::all();

        foreach ($appointments as $appointment) {
            $events[] = [
                'title' => $appointment -> client -> name. " ". $appointment -> client -> surname . " - " . $appointment ->service-> title ,
                'start' => $appointment -> date."T".$appointment->start,
                'end' => $appointment -> date."T".$appointment->end,
                'allDay' => false
            ];
        }
        return response($events);
    }
}
