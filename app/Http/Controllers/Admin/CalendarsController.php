<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class CalendarsController extends Controller
{
    public function index()
    {
        if (! Gate::allows('calendar_access')) {
            return abort(401);
        }
        return view('admin.calendars.index');
    }
}
