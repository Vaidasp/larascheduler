<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Client::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->firstName,
        'surname' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->e164PhoneNumber
    ];
});

$factory->define(App\Employee::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->firstName,
        'surname' => $faker->lastName,
    ];
});

$factory->define(App\Service::class, function (Faker\Generator $faker) {

    return [
        'title' => $faker->sentence($nbWords = 3, $variableNbWords = true),
        'price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 100),
        'description' => $faker->text($maxNbChars = 191),
    ];
});
